# Description

Prints environment variables containing a particular string.

## Usage

Import `Eagle` and invoke its static `print` method, passing `process.env` and a
string to match.

```typescript
import { Eagle } from "eagle-env";

Eagle.print(process.env, "MY_MATCH_STRING");
```

Example output:

```console
:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
MY_MATCH_STRING_PRODUCTION: false
MY_MATCH_STRING_API_KEY: abcd1234
:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
```
