import { Log } from "@ingentis/log";

import { EagleOptions } from "./eagle-options";

const DEFAULT_OPTIONS: EagleOptions = {
  decorations: {
    before: ":::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::",
    after: ":::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::",
  },
};

export class Eagle {
  public static print(
    env: any,
    match: string,
    options: EagleOptions = DEFAULT_OPTIONS
  ) {
    if (options?.decorations?.before) Log.info(options.decorations.before);
    for (const key in env) {
      if (key.indexOf(match) > -1) Log.info(`${key}: ${env[key]}`);
    }
    if (options?.decorations?.after) Log.info(options.decorations.after);
  }
}
