export interface EagleOptions {
  decorations?: {
    before?: string;
    after?: string;
  };
}
